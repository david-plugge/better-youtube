{
    const qualities = ['hd2160', 'hd1440', 'hd1080', 'hd720', 'large', 'medium', 'small', 'tiny', 'auto'];
    const script = document.currentScript;

    let config;
    let player;

    // log to console if user selected logging
    function log(...args) {
        if (config.logs === 'true') {
            console.log(...args);
        }
    }

    // set player playback quality
    function setPlaybackQuality(q) {
        player.setPlaybackQuality(q);
        player.setPlaybackQualityRange(q, q);
    }

    // find best fit for preferred quality
    function setPreferredQuality() {
        const availQualities = player.getAvailableQualityLevels();
        const currQuality = player.getPlaybackQuality();
        const preferredQuality = config.preferredQuality;

        if (preferredQuality === currQuality) {
            log('Skipping, preferred quality already set.');
            return;
        }
        if (preferredQuality === 'highest') {
            const best = availQualities[0];
            if (best === currQuality) {
                log('Skipping, preferred quality already set.');
                return;
            }
            setPlaybackQuality(best);
            return;
        }
        if (config.higher === 'true' && qualities.indexOf(currQuality) < qualities.indexOf(preferredQuality)) {
            log('Skipping, current quality is higher than preferred quality.');
            return;
        }
        if (availQualities.includes(preferredQuality)) {
            setPlaybackQuality(preferredQuality);
            log(`Preferred quality is available: ${preferredQuality}.`);
            return;
        }
        {
            for (let i = qualities.indexOf(preferredQuality); i >= 0; i--) {
                const q = qualities[i];
                if (availQualities.includes(q)) {
                    setPlaybackQuality(q);
                    log(`Switching to next better: ${q}.`);
                    return;
                }
            }
        }
        if (availQualities.length > 0) {
            const best = availQualities[0];
            setPlaybackQuality(best);
            log(`Switching to best available: ${best}.`);
            return;
        }
        log(`Skipping, could not determine best fit quality`);
    }

    // handle state change
    function onStateChange(e) {
        const state = e.data ?? e;

        if (state === 1) {
            if (player.setPlaybackQualityRange || player.setPlaybackQuality) {
                setPreferredQuality();
            }
        }
    }

    // handle config updates
    function update() {
        config = script.dataset;
        player.removeEventListener('onStateChange', onStateChange);
        if (config.once !== 'true') {
            player.addEventListener('onStateChange', onStateChange);
        }
        onStateChange(player.getPlayerState());

        // auto cinema mode
        if (config.cinema === 'true') {
            const isCinemaMode = document.querySelector('ytd-watch-flexy')?.getAttribute('theater') != null;
            if (!isCinemaMode) {
                setTimeout(() => {
                    const cinemaModeBtn = document.querySelector('.ytp-size-button');
                    cinemaModeBtn.click();
                    log('Switching to cinema mode.');
                }, 250);
            }
        }
    }

    // initialize player and handlers
    function onPlayerReady(_player) {
        if (player) return;
        player = _player;
        update();
    }

    window.onYouTubePlayerReady = onPlayerReady;
    window.addEventListener('spfready', () => {
        if (typeof window.ytplayer === 'object' && window.ytplayer.config) {
            window.ytplayer.config.args.jsapicallback = 'onYouTubePlayerReady';
        }
    });
    window.addEventListener('yt-navigate-finish', () => {
        const player = document.querySelector('.html5-video-player');
        if (player) {
            window.onYouTubePlayerReady(player);
        }
    });
    document.addEventListener('BYT-UPDATE', () => update());
}
