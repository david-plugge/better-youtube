const defaultPrefs = {
    preferredQuality: 'highest',
    once: false,
    logs: false,
    higher: true,
    cinema: false,
};

const script = document.createElement('script');

chrome.storage.local.get(defaultPrefs, (prefs) => {
    Object.assign(script.dataset, prefs);
    script.src = chrome.runtime.getURL('/js/code.js');
    script.onload = () => script.remove();
    (document.head || document.documentElement).appendChild(script);
});
chrome.storage.onChanged.addListener((prefs) => {
    Object.entries(prefs).forEach(([key, value]) => (script.dataset[key] = value.newValue));
    document.dispatchEvent(new CustomEvent('BYT-UPDATE'));
});
