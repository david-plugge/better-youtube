const defaultPrefs = {
    preferredQuality: 'highest',
    once: false,
    higher: true,
    cinema: false,
    logs: false,
};

const preferredQuality = document.getElementById('quality');
const once = document.getElementById('once');
const higher = document.getElementById('higher');
const cinema = document.getElementById('cinema');
const logs = document.getElementById('logs');
const reset = document.getElementById('reset');

const toastContainer = document.querySelector('.toasts');

function toast(message) {
    const t = document.createElement('div');
    t.textContent = message;
    toastContainer.appendChild(t);
    setTimeout(() => {
        t.remove();
    }, 1000);
}

function load() {
    chrome.storage.local.get(defaultPrefs, (prefs) => {
        console.log(prefs);
        preferredQuality.value = prefs.preferredQuality;
        once.checked = prefs.once;
        higher.checked = prefs.higher;
        cinema.checked = prefs.cinema;
        logs.checked = prefs.logs;
    });
}

reset.addEventListener('click', () => {
    chrome.storage.local.clear(() => {
        chrome.runtime.reload();
        window.close();
        toast('Reset');
    });
});

preferredQuality.addEventListener('input', () => {
    chrome.storage.local.set(
        {
            preferredQuality: preferredQuality.value,
        },
        () => {
            toast('Saved');
        },
    );
});

once.addEventListener('input', () => {
    chrome.storage.local.set(
        {
            once: once.checked,
        },
        () => {
            toast('Saved');
        },
    );
});

higher.addEventListener('input', () => {
    chrome.storage.local.set(
        {
            higher: higher.checked,
        },
        () => {
            toast('Saved');
        },
    );
});

cinema.addEventListener('input', () => {
    chrome.storage.local.set(
        {
            cinema: cinema.checked,
        },
        () => {
            toast('Saved');
        },
    );
});

logs.addEventListener('input', () => {
    chrome.storage.local.set(
        {
            logs: logs.checked,
        },
        () => {
            toast('Saved');
        },
    );
});

document.addEventListener('DOMContentLoaded', load);
