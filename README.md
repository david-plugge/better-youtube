# Better Youtube

`A browser plugin to make youtube just a little bit better.`

The plugin allows to set the quality of the videos and automatically switch to the cinema mode.

![Example settings](.assets/image.png)

## Licence

Copyright (c) 2022 David Plugge

This software is released under the terms of the MIT License.
See the [LICENSE](LICENSE) file for further information.
